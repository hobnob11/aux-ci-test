# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) 
{
     $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
     Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
}
else {
    $addonPath = $(Join-Path $PSScriptRoot "..\addons")
    function SymLink
    {
        param([string]$source, [string]$dest)
        Write-Verbose("Creating SymLink: $source > $destination")
        New-Item -ItemType SymbolicLink -Path $dest -Target $source -Force
    }
    if(Test-Path 'P:') #if P Drive Mounted, create symlinks and build from there. 
    {
        foreach($addonFolder in Get-ChildItem -Directory $addonPath)
        {
            SymLink -source "$addonPath\$addonFolder" -dest "P:\$addonFolder"
        }
    }
}
