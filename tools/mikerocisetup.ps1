
git clone --depth=1 "https://github.com/arma-actions/mikero-tools.git"

Get-ChildItem "${{ github.action_path }}/windows" | ForEach-Object {
    echo "Installing $_"
    Start-Process -Wait $_.FullName "/S"
}
echo "Updating environment variables"

$AddedLocation ="C:\Program Files (x86)\Mikero\DePboTools\bin"
$Reg = "Registry::HKLM\System\CurrentControlSet\Control\Session Manager\Environment"
$OldPath = (Get-ItemProperty -Path "$Reg" -Name PATH).Path
$NewPath= $OldPath + ’;’ + $AddedLocation
Set-ItemProperty -Path "$Reg" -Name PATH –Value $NewPath
$Env:Path = $Env:Path + ";" + $AddedLocation

echo "mikero setup complete?"
