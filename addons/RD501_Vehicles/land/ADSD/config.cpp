#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_adsd
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(cis,adsd)
		};
		weapons[]=
		{
			
		};
	};
};

class CfgVehicles
{
	class LandVehicle;
	class Tank: LandVehicle
	{
		class NewTurret;
	};
	class Tank_F: Tank
	{
		class Turrets
		{
			class MainTurret: NewTurret
			{};
		};
	};
	class 3AS_AAT_base_F: Tank_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
			};
		};
	};
    class 3AS_Advanced_DSD_Base:3AS_AAT_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
			};
		};
	};
	class macro_new_vehicle(cis,adsd): 3AS_Advanced_DSD_Base
    {
		scope = 2;
        displayName = "CIS ADSD";
        crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
        side = 0;
        faction = MACRO_QUOTE(macro_faction(CIS));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
        scopeCurator = 2;
        forceInGarage = 1;
		armor=1300;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[]=
				{
					macro_new_weapon(aat_cannon,king),
					"3AS_ADSD_Repeater"
				};
				magazines[]=
				{
					macro_new_mag(aat_mbt,10),
					macro_new_mag(aat_mbt,10),
					macro_new_mag(aat_mbt,10),
					macro_new_mag(aat_mbt,10),
					"3AS_500Rnd_ATT_RedPlasma",
					"3AS_500Rnd_ATT_RedPlasma"
				};
			};
		};
    };
};