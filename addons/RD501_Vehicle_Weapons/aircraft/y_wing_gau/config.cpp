
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_y_wing_gau
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(voltic,y_wing),
			macro_new_weapon(pylon_voltic,y_wing)
			
		};
	};
};

class CfgWeapons
{
	
	class CannonCore;

	class Gatling_30mm_Plane_CAS_01_F:CannonCore
	{
		class LowROF;
	};
	class macro_new_weapon(voltic,y_wing):Gatling_30mm_Plane_CAS_01_F
	{
		displayName = "GAU-7D Voltic";
		magazines[] = {
			macro_new_mag(voltic,200)
		};
		maxZeroing = 10;
		author= "RD501";
	
		ballisticsComputer = 1;
		
		irLock = 1;
		cmImmunity = 0;
		lockAcquire = 0;
		FCSMaxLeadSpeed = 1500;
		laserLock = 1;
		canLock = 2;
		weaponLockSystem = "2+4+8+16";
		magazineReloadTime=10; 
		
		class LowROF: LowROF
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				soundClosure[] = {"closure", 1};
			};
			class StandardSound
			{
				begin1[] = {""};
				begin2[] = {""};
				begin3[] = {""};
				begin4[] = {""};
				begin5[] = {""};
				soundBegin[] = {"begin1", 0.2, "begin2", 0.2, "begin3", 0.2, "begin4", 0.2, "begin5", 0.2};
			};
			burst = 10;
			soundContinuous=0;
			reloadTime = 0.04;
		};
	};
	class macro_new_weapon(pylon_voltic,y_wing) : macro_new_weapon(voltic,y_wing)
	{
		displayName = "Voltic (Pylon)";
		displayNameShort = "Voltic";
		magazines[]={macro_new_mag(pylon_voltic,200)};
		class LowROF: LowROF
		{
			burst = 1;
			soundContinuous=0;
			reloadTime = 0.04;
			dispersion=0.0025;
		};
	};
}; 

