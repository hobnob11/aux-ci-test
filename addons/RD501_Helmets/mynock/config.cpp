#define COMPONENT aviation_helmets
#define BASEHELMNAME MODNAME##_501st_Pilot_Base
#include "../../RD501_main/config_macros.hpp"
#include "config_macros.cpp"

class CfgPatches
{
	class macro_patch_name(mynock_helmets)
	{
		author=MACRO_QUOTE(DANKAUTHORS);
		addonRootClass=MACRO_QUOTE(macro_patch_name(helmets));
		requiredAddons[]=
		{
			MACRO_QUOTE(macro_patch_name(helmets))
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={};
	};
};

class CfgWeapons
{
	class macro_new_helmet(mynock,base_3as);
	NEW_501_MYN_Helm_3AS(cic,CI-C,501st_Tanker_CI-C.paa)
	NEW_501_MYN_Helm_3AS(ci,CI,501st_Tanker_CI.paa)
	NEW_501_MYN_Helm_3AS(cmc,CM-C,501st_Tanker_CM-C.paa)
	NEW_501_MYN_Helm_3AS(cm,CM,501st_Tanker_CM.paa)
	NEW_501_MYN_Helm_3AS(ct,CT,501st_Tanker_CT.paa)
	NEW_501_MYN_Helm_3AS(cp,CP,501st_Tanker_CP.paa)
	NEW_501_MYN_Helm_3AS(cs,CS,501st_Tanker_CS.paa)
};