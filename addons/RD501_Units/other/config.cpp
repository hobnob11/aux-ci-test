#include "../config_macros.hpp"
#include "config_macros.hpp"
#include "../../RD501_main/config_macros.hpp"
//

#define macro_new_glass(className) CONCAT_3(MODNAME,_,className)

class CfgPatches
{
	class RD501_patch_other_units
	{
		addonRootClass=RD501_patch_units;
		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_units)
		};
		requiredVersion=0.1;
		units[]={
			

		};
		weapons[]=
		{
			macro_new_glass(Diving_Goggles),
			macro_new_glass(phase2_hud),
			macro_new_glass(phase2_hud_snow),
			macro_new_glass(phase1_hud),
			macro_new_glass(acklay_hud),
			macro_new_glass(dark_mask_2)
		};
	};
};

class CfgGlasses
{
	class G_B_Diving;
	class G_Diving;
	class g_balaclava_ti_blk_f;
	class g_balaclava_blk;

	class macro_new_glass(Diving_Goggles): G_B_Diving
	{
		author = "rd501";
		displayname = "Republic Underwater Display";
		scope=2;
		scopeArsenal=2;
		picture = "\A3\Characters_F_EPB\Heads\Glasses\data\UI\icon_g_diving_nato_ca.paa";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {""};
		mass = 0.1;
	};
	class macro_new_glass(remove_blue): macro_new_glass(Diving_Goggles)
	{
		displayName = "Color Suppression Chip (Blue)";
		author = macro_quote(DANKAUTHORS);
		picture = "\RD501_Units\textures\icons\no_blue.paa";
		scope = 2;
		scopeCurator = 2;
		scopeArsenal = 2;
		modelOptics="";

		ace_color[] = {0.0,0.0,-200.0};
		ace_tintAmount = 1;
		ace_resistance = 1;
		ace_protection = 0;
		ACE_Overlay = "";
		ace_overlayDirt = "";
		ace_dustPath = "";
		ACE_OverlayCracked = "";
	};
	class macro_new_glass(remove_red): macro_new_glass(remove_blue)
	{
		displayName = "Color Suppression Chip (Red)";
		picture = "\RD501_Units\textures\icons\no_red.paa";
		ace_color[] = {-150.0,0.0,0.0};
	};
	class macro_new_glass(remove_green): macro_new_glass(remove_blue)
	{
		displayName = "Color Suppression Chip (Green)";
		picture = "\RD501_Units\textures\icons\no_green.paa";
		ace_color[] = {0.0,-100.0,0.0};
	};
	class 501st_inf_acc_01_a
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 01 A";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\sr_nco_acc.paa};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_01_b
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 01 B";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsMaterials[]= {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender.rvmat","\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_cloth.rvmat"};
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_co.paa"};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
	};
	class 501st_inf_acc_01_c
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 01 C";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"", TEXTUREPATH\Republic\clones\Infantry\acc\trooper_heavy_acc.paa};
		hiddenSelectionsMaterials[]= {""};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_01_c_medic
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 01 C (Medic)";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"",TEXTUREPATH\Republic\clones\Infantry\acc\medic_heavy_acc.paa};
		hiddenSelectionsMaterials[]= {""};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_01_c_rto
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 01 C (RTO)";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"",TEXTUREPATH\Republic\clones\Infantry\acc\rto_heavy_acc.paa};
		hiddenSelectionsMaterials[]= {""};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_02
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 02";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_03
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 03";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
		hiddenSelections[] = {"camo2"};
		hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\sr_nco_acc.paa};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_03_b
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 03 Medic";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
		hiddenSelections[] = {"camo2"};
		hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\inf_medic_nco.paa};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
	class 501st_inf_acc_03_c
	{
		ACE_Overlay = "RD501_Units\textures\glasses\phase2.paa";
		ACE_OverlayCracked = "RD501_Units\textures\glasses\phase2crack.paa";
		ace_overlayDirt = "RD501_Units\textures\glasses\phase2dirt.paa";
		ACE_Resistance = 2;
		displayname = "[501st] INF ACC 03 RTO ";
		identityTypes[] = {};
		mass = 4;
		model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
		hiddenSelections[] = {"camo2"};
		hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\inf_rto_nco.paa};
		name = "None";
		scope = 2;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
	};
};